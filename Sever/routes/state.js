var express = require('express');
var router = express.Router();
var db = require('../db');
var db2 = require('../db2');
var DriverModel= require('../model/DriverModel');
/* GET users listing. */
// router.get('/', function(req, res, next) {
//   db.loadPicks(res);
// });

router.get('/not_geo', function(req, res, next) {
    // db.loadPicksNotGeo().then(data => {
    //   res.send(data);
    // });
    db2.loadPicksNotGeo().then(data =>{
        console.log(data);
        res.send(data);
    });
});

router.get('/', function(req, res, next) {
    db2.loadPicks().then(data=>{
        res.send(data);
    });
});

router.get('/:id', (req, res) => {
    console.log(req.params.id);
    //db.loadPickWithId(req.params.id, res);
    db2.loadPickWithId(req.params.id, res);
});
router.post('/:id',(req,res)=>{
    db2.updateStateBooked(req.params.id,req.body.id).then(data=>{
        res.status(200).json({message: "updated"});
    })
})
router.put('/geo/:id', function(req, res) {
    var id = parseInt(req.params.id, 10);
    console.log(req.body);
    db2.updateStateGeo(id, req.body).then(message => {
        req.io.sockets.emit("updated",{message: 'data updated'});
        res.json({mes : message});
    });
    // var driverIdList = DriverModel.createDriverIdList(req.driversSocketId);
    var DriverPromise= DriverModel.getAvailableDriver(req.driversSocketId,req.body);
    Promise.all(DriverPromise).then(drivers=>{
        AvailableDriver= drivers.sort((a,b)=>{
            return a.distance - b.distance;
        });
        DriverModel.sendData(req.driverSocket,drivers, req.body);
    })
});
module.exports = router;
