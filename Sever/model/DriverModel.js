var db2 = require('../db2.js');
const io = require('socket.io')();

// function createDriverIdList(socketList){
// 	var DriverIdList=[]
// 	socketList.forEach( function(element) {
// 		// statements
// 		DriverIdList.push(element.customId);
// 	},this);
// 	console.log(DriverIdList);
// 	return DriverIdList;
// }
function createDistanceByDriverObj(driverID, place)
{
	return new Promise((resolve)=>{
		var distanceByDriver= new Object();
		db2.loadDriverWithId(driverID.customId)
		.then(driver=>{
			distanceByDriver.socketId = driverID.socketId;
			distanceByDriver.driver = driver;
			distanceByDriver.distance = getDistanceFromLatLonInKm(driver.current_lat, driver.current_lng, place.current_lat, place.current_lng);
			resolve(distanceByDriver);
		})
	})
}
function getAvailableDriver(driverIdList, place){
	driver = [];
	driverIdList.forEach( (element)=>{
		driver.push(createDistanceByDriverObj(element, place));
	});
	return driver;
}
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}
function sent(socket, driver, place)
{
	console.log("in SOCKET", place);
	socket.to(driver.socketId).emit('booking',place);
}
function sendData(socket, driverList, place)
{	
	// setInterval(function(){
	// 	socket.emit('booking',)
	// }, 5000)
	var i=0;
	var send= setInterval(function(){
		console.log(driverList[i]);
		db2.loadPicksId(place.id).then(updatedState=>{
			console.log("checking");
			if ((updatedState.state === "Booked")||(driverList[i].distance>1))
			{
				clearInterval(send);
			}
			else{
				sent(socket,driverList[i], place);
				i++;
				if (i === driverList.length){
					db2.updateStateFailed(place.id);
					clearInterval(send);
				}
			}
		});
	} , 5000);	
}
function deg2rad(deg) {
  return deg * (Math.PI/180)
}
module.exports = { getDistanceFromLatLonInKm, deg2rad, getAvailableDriver, sendData}