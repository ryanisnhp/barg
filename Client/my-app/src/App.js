import React from "react";
import { render } from "react-dom";
import axios from 'axios';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

import dataChange from "./socketHandle.js";
import MyMapComponent from "./Map.js"

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [{ID:1, Name:"a", Type:"b", CurrentLoc:"Current Location", State:"State", Note: "Note"}],
      picks: {}    
    };
    dataChange(()=> this.componentDidMount("ok") )
  }

  fetchData() {
  }
  componentDidMount(err){
    axios.get("http://www.localhost:3000/state").then(res=>{
      console.log(res);
        this.setState(res)
      }
    );
  }

  showMap(id){
    axios.get("http://www.localhost:3000/state/"+id).then(res=>{
        this.setState(res.data)
      }
    );
  }
  render() {
    const { data, picks } = this.state;
    console.log(picks)
    return (
      <div>
        <MyMapComponent picks={picks}/>
        <ReactTable
          data={data}
          columns={[
            { Header: "ID",
              accessor: "id",
              Cell: row => (
                  <div id="${row.value}$" onClick= {(e)=> this.showMap(row.value,e)} >
                  {row.value}
                  </div>
                )},
            { Header: "Name",
              accessor: "name"},
            { Header: "Type",
              accessor: "type"},
            { Header: "Current Location",
              columns: [
                  { Header: "Latitude",
                    accessor: "current_lat" },
                  {
                    Header: "Longitude",
                    accessor: "current_lng"
                  },
                ]
              },
            { Header: "State",
              accessor: "state"},
            { Header: "Note",
              accessor: "note"},
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
        />
        <br />
      </div>
    );
  }
}

export default App;
