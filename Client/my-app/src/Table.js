import React from "react";
import { render } from "react-dom";
import axios from 'axios';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

import dataChange from "./socketHandle.js";

class Table extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state={
    	data: props.data
    }
	// dataChange(()=> this.componentDidMount("ok") )
	}

 	componentDidMount(err){
    axios.get("http://www.localhost:3000/state").then(res=>{
        this.setState(res)
      }
    );
  }
	render() {
    const { data } = this.state;
    return (
      <div>
        <ReactTable
          data={data}
          columns={[
            { Header: "ID",
              accessor: "id",
              Cell: row => (
                  <div id="${row.value}$" onClick= {(e)=> this.props.showMap(row.value,e)} >
                  {row.value}
                  </div>
                )},
            { Header: "Name",
              accessor: "name"},
            { Header: "Type",
              accessor: "type"},
            { Header: "Current Location",
              columns: [
                  { Header: "Latitude",
                    accessor: "current_lat" },
                  {
                    Header: "Longitude",
                    accessor: "current_lng"
                  },
                ]
              },
            { Header: "State",
              accessor: "state"},
            { Header: "Note",
              accessor: "note"},
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
        />
        <br />
      </div>
    );
  }


}

export default Table;
