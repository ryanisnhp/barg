//socket
import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:3000');

function dataChange(cb) {
	socket.on('updated', () => cb(null));
}

export default dataChange;
