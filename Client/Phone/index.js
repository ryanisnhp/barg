const express = require('express');
	  bodyParser = require('body-parser');
	  morgan = require ('morgan');
      path = require('path');
      
let app = express();
let Phone=require("./routes/Phone");


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))


app.use(morgan('dev'));

app.use(express.static(
    path.resolve(__dirname, 'public')
));

// app.use('/',(req,res)=>{
// 	res.sendFile('Phone.html',{
//         root: __dirname
//     });
// });

app.use('/',Phone);
const Port = 3001;
app.listen(Port , ()=>{
	console.log("running");
})