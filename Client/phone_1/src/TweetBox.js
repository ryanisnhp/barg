import React, {Component} from 'react';

class TweetBox extends Component{
    constructor(props){
        super(props);
        this.state = {
            text: "",
        };
    }
    handleChange(text){
        this.setState({ text: text});
    }

    submitInfo(){
      console.log("Submit info");
      
    }

    render() {
        return (
            <div class="container">
            <form class="form-horizontal" id="phone-form" >
            <div class="form-group">
              <label class="control-label col-sm-2" for="text">Tên:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="Name" placeholder="Nhập tên"/>
	            </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="Address">Số điện thoại:</label>
              <div class="col-sm-8"> 
                <input type="text" class="form-control" id="Phone" placeholder="Nhập số điện thoại"/>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="Address">Địa chỉ:</label>
              <div class="col-sm-8"> 
                <input type="text" class="form-control" id="Address" placeholder="Nhập địa chỉ"/>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-sm-2" for="Type">Loại xe:</label>
              <div class="col-sm-8">
                <div class="radio">
                  <label><input type="radio" id="Type" name="optradio" value="normal"/>Thường</label>
                </div>
                <div class="radio">
                  <label><input type="radio" id="Type" name="optradio" value="premium"/>Premium</label>
                </div>
              </div>
            </div>

            <div class="form-group"> 
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary" onClick={() => this.submitInfo()}>Submit</button>
                <button type="history" class="btn btn-default" onClick = {() => console.log("History")}>History</button>
              </div>
            </div>
            </form>
          </div>
        );
    }
}

export default TweetBox;
