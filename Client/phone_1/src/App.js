import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TweetBox from './TweetBox';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      tweets: []
    }
  }

  handleTweet(tweetText) {
    let tweetObj = {
      text: tweetText
    }
    this.setState({
      tweets: this.state.tweets.concat(tweetObj)
    });
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to BARG</h1>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
	        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </header>
        <body>
          <br/>
          <TweetBox onTweet={this.handleTweet.bind(this)}></TweetBox>
        </body>
      </div>
    );
  }
}

export default App;