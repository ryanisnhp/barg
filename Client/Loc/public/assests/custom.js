var selectedGuest;
var img= './assests/bike.png';
function addPoint(element)
{
	var latLng ={
		lat: element.current_lat,
		lng: element.current_lng
	};
    var marker = new google.maps.Marker({
        position: latLng,
        map: map
  	});
}
function addDriver(element)
{
	var latLng ={
		lat: element.current_lat,
		lng: element.current_lng
	};
	let bikeicon={
		url: img,
		scaledSize: new google.maps.Size(50, 50), // scaled size
	    origin: new google.maps.Point(0,0), // origin
	    anchor: new google.maps.Point(0, 0), // anchor
	};
    var marker = new google.maps.Marker({
        position: latLng,
        icon: bikeicon,
        map: map
  	});
}
function addRows(element){
	var tr = "<tbody>";
	var rowId = '<tr><td>' + element.id + '</td>';
	var rowName = '<td><a onclick="myFunction(' + element.id + ')">' + element.name + '</a></td>';
	//
	var rowType = '<td></td>';
	if(element.type.toUpperCase() === 'PREMIUM'){
		rowType = '<td><b>' + element.type.toUpperCase() + '</b></td>';
	}else if(element.type.toUpperCase() == 'NORMAL'){
		rowType = '<td>' + element.type.toUpperCase() + '</td>';
	}
	var rowNote = '<td>' + element.note + '</td></tr>';
	tr = tr + rowId + rowName + rowType + rowNote;
	tr += '</tbody>';
	$('#table-retrieve').append(tr);

}
function myFunction(id){
	console.log("Click on id", id);
	deleteMarker();
	loadPickWithId(id);
}

function loadData(){
		$.ajax({
		url: 'http://localhost:3000/state/not_geo',
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		timeout: 10000
	}).done(function(data){
		console.log(data);
		data.forEach(function(element) {
			addRows(element);
			//
			//addPoint(element);
		}, this);
	}).fail(function(jqXHR, textStatus, error){
		console.log(textStatus);
		console.log(error);
		console.log(jqXHR);
	});

}
var notgeoObj = {};
function loadPickWithId(id){
		$.ajax({
		url: 'http://localhost:3000/state/' + id,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		timeout: 10000
	}).done(function(data){
		console.log(data);
		notgeoObj = data;
		geoCode(data.picks.note);
	}).fail(function(jqXHR, textStatus, error){
		console.log(textStatus);
		console.log(error);
		console.log(jqXHR);
	});
}
var places = [];
function geoCode(rawAddress){
	let baseurl = 'http://localhost:3000/geocoder/geo?loc=' + encodeURIComponent(rawAddress + ' ho chi minh');
	console.log(baseurl);
	$.ajax({
		url: baseurl,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		timeout: 10000
	}).done(function(data){
		console.log(data);
		if (data.length > 0){
			updateToMarker(data);
			updateToPlacePickers(data);
			moveToLocation({
			lat: data[0].geometry.location.lat,
			lng: data[0].geometry.location.lng,
		})
		data.forEach(place=>{
			let obj={
				lat: place.geometry.location.lat,
				lng: place.geometry.location.lng,
				formatted_address: place.formatted_address,
			}
			places.push(obj);
		},this);
		let guest=notgeoObj.picks;
		guest.current_lat=data[0].geometry.location.lat;
		guest.current_lng=data[0].geometry.location.lng;
		console.log('NOT GEO OBJ');
		console.log(guest);
		loadDriverList(guest);
		}else{
			console.log(data.message);
		}
	}).fail(function(jqXHR, textStatus, error){
		console.log(textStatus);
		console.log(error);
		console.log(jqXHR);
	});
}
function updatePickedPlace(pickedPlace){
	
}
function addPlaceToPlacePickers(place)
{
	var tr = "";
	var rowCount = $('#table-geocode').children('tr').length;
	rowId = '<tr><td><input type="radio" name="radioName" value="'+ rowCount +'" /></td>';
	rowName = '<td id="'+rowCount+'">' + place.formatted_address + '</td>';
	rowLocation = '<td>' + place.lat + ', ' + place.lng + '</td></tr>';
	tr = tr + rowId + rowName + rowLocation;
	$('#table-geocode').append(tr);

	var rad = document.getElementsByName('radioName');
	rad[rad.length-1].onclick=function(){
		console.log(this.value);
	}
}

function updateToPlacePickers(places){
	$('#table-geocode').text('');
	var tr = "";

	for(var i = 0; i < places.length; i++){
		rowId = '<tr><td><input type="radio" name="radioName" value="'+ i +'" /></td>';
		rowName = '<td id="'+i+'">' + places[i].formatted_address + '</td>';
		rowLocation = '<td>' + places[i].geometry.location.lat + ', ' + places[i].geometry.location.lng + '</td></tr>';
		tr = tr + rowId + rowName + rowLocation;
	}
	$('#table-geocode').append(tr);
	

	var rad = document.getElementsByName('radioName');
    var prev = null;
    for(var i = 0; i < rad.length; i++) {
        rad[i].onclick = function() {
            console.log(this.value);
        };
    }

	//save button
	$('#save-button').text('');  
	var button = '<button type="button" class="btn btn-primary btn-md" onclick="chooseGeocode()">Save</button>';
	$('#save-button').append(button);
}

function updateToMarker(places){
	places.forEach(place =>{
		var marker = new google.maps.Marker({
        	position: {
        		lat: place.geometry.location.lat,
        		lng: place.geometry.location.lng,
        	},
        	map: map,
  		});
  		clickedPosition.push(marker);
	},this);
}

function moveToLocation(Loc)
{
	map.setCenter(Loc);
	map.setZoom(15);

}
function updateState(obj){
	$.ajax({
		url: 'http://localhost:3000/state/geo/' + obj.id,
		type: 'PUT',
		data: JSON.stringify(obj),
		dataType: 'json',
		contentType: 'application/json',
		timeout: 10000
	}).done(function(data){
		$('#table-geocode').text('');
		$('#save-button').text('');  
	}).fail(function(jqXHR, textStatus, error){
		console.log(textStatus);
		console.log(error);
		console.log(jqXHR);
	});
}

function loadDriverList(obj)
{
	loadDriver(obj, 0.3).then(drivers=>{
	driverList=drivers;
	console.log(driverList);
	if (driverList.length<10)
	{
		loadDriver(obj,0.6).then(drivers=>{
			driverList = drivers;
			console.log(driverList);
			if (driverList.length<10)
			{
				loadDriver(obj,1).then(drivers=>{
				driverList = drivers;
				console.log(driverList);
				driverList.forEach(element=>{
					console.log(element);
					addDriver(element);
					})
				});
			}
			else {
				driverList.forEach(element=>{
					console.log(element);
					addDriver(element);
				})
			}
		})
	}
	else{
		driverList.forEach(element=>{
		console.log(element);
		addDriver(element);
		})
		}
	});

}

function loadDriver(data, distance)
{	
	return new Promise((resolve, reject) => {
		var position = {
				lat: data.current_lat,
				lng: data.current_lng,
				type: data.type,
				max_distance: distance
			}
		console.log(position);
		$.ajax({
			url: 'http://localhost:3000/drivers',
			type: 'GET',
			data: position,
			contentType: 'application/json',
			timeout: 10000
		}).done(data=>{
			resolve(data.top_10_nearest);
		}).fail(function(jqXHR, textStatus, error){
			console.log(textStatus);
			console.log(error);
			console.log(jqXHR);
			reject(error);
		});
	})
}

$( document ).ready(function() {
	loadData();
});


function chooseGeocode(){
	let driverList=[];
	var index = $('input[name=radioName]:checked').val();
	var formatted_address = $('#'+index).text();
	//console.log(places[index].formatted_address);
	var obj = notgeoObj.picks;
	obj.current_lat = places[index].lat;
	obj.current_lng = places[index].lng;
	obj.state = "GEO";
	obj.formatted_address= formatted_address;
	console.log(obj);
	updateState(obj);
	deleteMarker();
}
function setMapOnAll(map) {
	for (var i = 0; i < clickedPosition.length; i++) {
	  clickedPosition[i].setMap(map);
	}
}

function deleteMarker()
{
	setMapOnAll(null);
	console.log(clickedPosition);
	clickedPosition=[];
}
function selectPlace(position){

    var marker = new google.maps.Marker({
        position: position,
        map: map
  	});
  	clickedPosition.push(marker);

}

function reverseGeoCode(place){
	return new Promise((resolve, reject) =>{
		$.ajax({
			url: 'http://localhost:3000/geocoder/reversegeo',
			type: 'GET',
			dataType: 'json',
			data: place,
			contentType: 'application/json',
			timeout: 10000
		}).done(function(data){
			resolve(data.formatted_address);
		}).fail(function(jqXHR, textStatus, error){
			console.log(textStatus);
			console.log(error);
			console.log(jqXHR);
			reject(error);
		});
	})
}
