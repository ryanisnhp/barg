const express = require('express'),
    morgan = require('morgan'),
    path = require('path');

let app = express();

app.use(morgan('dev'));
app.use(express.static(
    path.resolve(__dirname, 'public')
));

app.get('/state', (req, res) => {
    res.sendFile('index.html', {
        root: __dirname
    });
});

app.get('/state/:id', (req, res) => {
    res.sendFile('detail.html', {
        root: __dirname
    });
});

const PORT = 3003;
app.listen(PORT, () => {
    console.log('client-site run on port: ' + PORT);
});