$( document ).ready(function() {
	var repString = document.URL.replace("3003", "3000");
	$.ajax({
		url: repString,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		timeout: 10000
	}).done(function(data){
		console.log(document.URL);
		//customer and driver
		$('#customer').append(loadPickCustomer(data));
		$('#driver').append(loadPickDriver(data));

		//mappp
		var options = {
			center: {
				lat: data.picks.current_lat,
				lng: data.picks.current_lng
			},
			zoom: 15
		},
		element = document.getElementById('map-canvas'),
		map = new google.maps.Map(element, options);
		infowindow = new google.maps.InfoWindow;
		//direction
		var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
		directionsDisplay.setMap(map);
        direction(data, directionsService, directionsDisplay);

		//passenger marker
		// marker = new google.maps.Marker({
		// 	position: { 
		// 		lat: data.picks.current_lat,
		// 		lng: data.picks.current_lng
		// 	}
		// });
		// marker.setMap(map);
		infowindow.setContent('Passenger : ' + data.picks.name);
		infowindow.open(map, marker);
		//driver marker
		if(data.picks.driver != null){
			console.log(data.picks.driver.current_lat);
			// markerDriver = new google.maps.Marker({
			// 	position: { 
			// 		lat: data.picks.driver.current_lat,
			// 		lng: data.picks.driver.current_lng
			// 	}
			// });
			// markerDriver.setMap(map);
			// infowindow.setContent('Driver : ' + data.picks.driver.driver);
			// infowindow.open(map, markerDriver);
		}

	}).fail(function(jqXHR, textStatus, error){
		console.log(textStatus);
		console.log(error);
		console.log(jqXHR);
		// var alert = '<tr><th><div class="alert alert-danger">There is no card with id like this !</div></th></tr>';
		// $('#table-retrieve').append(alert);
	});
});

function loadPickCustomer(data){
	var tr = '<p></p>';
	var name = '<h4 class="name">' + data.picks.name + '</h4>';
	var idPick = '<p class="idPick"><b>ID: </b>' + data.picks.id + '</p>';
	//
	var state = '';
	if (data.picks.state == 'GEO'){
		state = '<p class="state">' + data.picks.state + ' <span class="glyphicon glyphicon-map-marker"></span></p>';
	}else if(data.picks.state == 'NOT GEO'){
		state = '<p class="state">' + data.picks.state + ' <span class="glyphicon glyphicon-ban-circle"></span></p>';
	}else if(data.picks.state == 'PICKED'){
		state = '<p class="state">' + data.picks.state + ' <span class="glyphicon glyphicon-arrow-right"></span></p>';
	}
	//
	var type = '';
	if(data.picks.type.toUpperCase() == 'PREMIUM'){
		type = '<p class="type"><b>' + data.picks.type.toUpperCase() + '</b></p>';
	}else if(data.picks.type.toUpperCase() == 'NORMAL'){
		type = '<p class="type">' + data.picks.type.toUpperCase() + '</p>';
	}

	var note = '<p class="note"><b>NOTE: </b>' + data.picks.note + '</p>';

	tr = tr + name + idPick + state + type + note;
	return tr;
}

function loadPickDriver(data){
	var trDriver = '<p></p>';
	if(data.picks.driver != null){
		var driverName = '<h4 class="driver-name">' + data.picks.driver.driver + '</h4>';
		var driverId = '<p class="driver-id"><b>ID: </b>' + data.picks.driver.id + '</p>';
		//
		var driverState = '';
		if (data.picks.driver.state == 'NO AVAILABLE'){
			driverState = '<p class="driver-state">' + data.picks.driver.state + '</p>';
		}else if(data.picks.driver.state == 'AVAILABLE'){
			driverState = '<p class="driver-state"><b>' + data.picks.driver.state + '</b></p>';
		}
		//
		var driverType = '';
		if(data.picks.driver.type.toUpperCase() == 'PREMIUM'){
			driverType = '<p class="type"><b>' + data.picks.driver.type.toUpperCase() + '</b></p>';
		}else if(data.picks.driver.type.toUpperCase() == 'NORMAL'){
			driverType = '<p class="type">' + data.picks.driver.type.toUpperCase() + '</p>';
		}

		trDriver = trDriver + driverName + driverId + driverState + driverType;
	}
	return trDriver;
}

function direction(data, directionsService, directionsDisplay){
	if(data.picks != null){
		customerLatLng = {lat : data.picks.current_lat, lng : data.picks.current_lng};
		if (data.picks.driver != null){
			driverLatLng = {lat : data.picks.driver.current_lat, lng : data.picks.driver.current_lng};
			//
			directionsService.route({
				origin: driverLatLng,
				destination: customerLatLng,
				travelMode: 'DRIVING'
			}, function(response, status) {
				if (status === 'OK') {
					directionsDisplay.setDirections(response);
				} else {
					window.alert('Directions request failed due to ' + status);
				}
			});

		}
	}
}
