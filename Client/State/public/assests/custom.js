$( document ).ready(function() {
	$.ajax({
		url: 'http://localhost:3000/state',
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		timeout: 10000
	}).done(function(data){
		var tr = "<tbody>";
		data.forEach(function(element) {
			var rowId = '<tr><td>' + element.id + '</td>';
			var rowName = '<td><a href = "http://localhost:3003/state/'+ element.id +' ">' + element.name + '</a></td>';
			//
			var rowType = '<td></td>';
			if(element.type.toUpperCase() == 'PREMIUM'){
				rowType = '<td><b>' + element.type.toUpperCase() + '</b></td>';
			}else if(element.type.toUpperCase() == 'NORMAL'){
				rowType = '<td>' + element.type.toUpperCase() + '</td>';
			}
			//
			var rowLocation = '';
			//
			var rowState = '<td></td>';
			if ((element.state === 'GEO')||(element.state === 'Booked')){
				rowState = '<td>' + element.state + ' <span class="glyphicon glyphicon-map-marker"></span></td>';
				rowLocation = '<td>( ' + element.current_lat + ', ' + element.current_lng + ' )<a href = "#">  See on map <span class="glyphicon glyphicon-globe"></span></a></td>';
			}else if(element.state == 'NOT GEO'){
				rowLocation = '<td><span class="glyphicon glyphicon-globe"> NOT GEO</span></td>';
				rowState = '<td>' + element.state + ' <span class="	glyphicon glyphicon-ban-circle"></span></td>';
			}else if(element.state == 'PICKED'){
				rowState = '<td>' + element.state + ' <span class="glyphicon glyphicon-arrow-right"></span></td>';
				rowLocation = '<td>( ' + element.current_lat + ', ' + element.current_lng + ' )<span class="glyphicon glyphicon-globe"></span></td>';
			}else if(element.state === 'FAILED'){
				rowLocation = '<td><span class="glyphicon glyphicon-globe"> FAILED</span></td>';
				rowState = '<td>' + element.state + ' <span class="	glyphicon glyphicon-ban-circle"></span></td>';
			}
			//
			var rowNote = '<td>' + element.note + '</td></tr>';
			tr = tr + rowId + rowName + rowType + rowLocation + rowState + rowNote;
		}, this);
		tr += '</tbody>';
		$('#table-retrieve').append(tr);
	}).fail(function(jqXHR, textStatus, error){
		console.log(textStatus);
		console.log(error);
		console.log(jqXHR);
		var alert = '<tr><th><div class="alert alert-danger">There is no card with id like this !</div></th></tr>';
		$('#table-retrieve').append(alert);
	});
});
